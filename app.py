"""Getting the best of both worlds:
Creating a dash app wrapped up in Flask so we can use those plugins in future    
"""

# First define as Flask app so we can use plugins
import flask
import dash
import dash_uploader as du  # better file upload
# import mod-dash-uploader as du  # better file upload
# if du.utils.dash_version_is_at_least("2.0.0"):
#     from dash import html  # if dash <= 2.0.0, use: import dash_html_components as html
# else:
#     import dash_html_components as html


from dash.dependencies import Input, Output, State
# import dash_html_components as html
# import dash_core_components as dcc
from dash import html
from dash import dcc
# 3rd party libraries
import dash_mantine_components as dmc
from dash_uploader import Upload
from dash.exceptions import PreventUpdate
import os
from cloudStorage import upload_blobs
import uuid
# from dash_uploader import UploadStatus

server = flask.Flask(__name__)


# route for dash app
# invokes my_dash_app() upon visiting /dash-yeastgen
# which, we'll need to set this to deploy to patrickfinnerty.com/Tools/ to work.
# @server.route('/Tools/dash-yeastgen')
# @server.route('/Tools/dash-yeastgen/')
def my_dash_app():
    return app.index()


# now define dash app that's called by flask app
app = dash.Dash(__name__)
server = app.server

# app = dash.Dash(
#     __name__,
#     server=server,  # use flask app as underlying server, enable plugins beyond dash
#     # url_base_pathname='/Tools/'  # we'll be embedding this with iframes :)
# )

##### VARIOUS DECLARATIONS FOR COMPONENT FUNCTION BELOW #####

# Use a server-side file system to store the uploaded files temporarily.
# TMP_DIR = tempfile.mkdtemp()

# Define the GCS bucket name.
BUCKET_NAME = 'yeastgen-fasta'

# this dir is used also by the cloudStorge fn
# be aware this should match the GCP bucket uhh directory stuff
# https://github.com/np-8/dash-uploader/tree/stable
du.configure_upload(app, 'jobs/', )  # use_upload_id = False)

# should hopefully set to global dark mode
component = dmc.MantineProvider(
    withGlobalStyles=True, theme={"colorScheme": "dark"})

# APP SECTIONS

# Define the input section of the layout using dash-mantine-components
input_section = dmc.TextInput(
    id='input',
    label='Enter a single FASTA sequence, or upload FASTA file below',
    placeholder='Data goes here :)'
)

# Define the upload section of the layout using dash-uploader
# https://github.com/np-8/dash-uploader/blob/dev/usage.py
upload_section = html.Div([
    html.P('Upload your files:'),
    du.Upload(
        id='dash-uploader',
        text_completed='Upload completed',
        filetypes=['csv', 'fasta', 'pdf'],  # ['bam', 'sam', 'vcf'],
        cancel_button=True,
        pause_button=True,
        max_file_size=200,  # 200 MB
        # max_total_size=400,
        max_files=10,
        upload_id=uuid.uuid4(),  # NOTE: RECOMMENT IF WANT DIFFERENT FOLDERS
    ),
    html.Div(id='callback-output'),
], style={
    'background-color': '#333333',
    'color': '#F3F3F3',
    'font-family': 'sans-serif',
})

# Define the submit button section of the layout using dash-mantine-components
submit_button_section = dmc.Button(
    children='Submit',
    id='submit'
)

# Define the output section of the layout using dash-core-components
output_section = html.Div(id='output')

# Combine the sections into the app layout using a html.Div
app.layout = html.Div([
    # input_section, # not using this for now, just the file upload :)
    upload_section,
    html.Br(),  # vspace
    # submit_button_section,
    # output_section,
])

# CALLBACKS
# Define the callback to handle file uploads
"""
New in version 0.7.0: Functions **DECORATED** with @du.callback must take status
(du.UploadStatus) object as the only input parameter (previously, filenames)
"""
# documentation for this object here:
# https://github.com/np-8/dash-uploader/blob/dev/docs/dash-uploader.md#3-callbacks
# https://github.com/np-8/dash-uploader/blob/dev/usage.py


@du.callback(
    output=Output("callback-output", "children"),
    id="dash-uploader",
)
def callback_on_completion(filenames):  # status: du.UploadStatus):
    # see cloudStorage.py for this fn
    upload_blobs(BUCKET_NAME, filenames)  # status.uploaded_files)
    # this next one should return all the files uploaded
    return html.Div([
        html.P('Uploaded files, location in bucket:'),
        html.Ul([html.Li(str(x)) for x in filenames])
    ])


# COMMENT DEBUG/tools OUT WHEN DEPLOYING TO PRODUCTION
if __name__ == '__main__':
    app.run(
        # debug=True,
        # dev_tools_ui=True
    )

"""
@du.callback(

)
@app.callback(
    Output('output', 'children'),
    Input('upload', 'filename'),
    State('upload', 'filecontents'))
def upload_file_to_gcs(n_clicks, filename, filecontents):
    # if ain't click
    if not n_clicks:
        raise PreventUpdate
    if not filename or not filecontents:
        return 'No file uploaded or file empty.'
    # Save the uploaded file temporarily to the server-side file system.
    file_path = os.path.join(TMP_DIR, filename)
    with open(file_path, 'wb') as f:
        f.write(filecontents)

    # Upload the file to GCS.
    upload_blob(BUCKET_NAME, file_path, "jobs"+filename)

    # blob = bucket.blob(filename)
    # blob.upload_from_filename(file_path)

    # Return a success message.
    return f'{filename} uploaded to GCS bucket {BUCKET_NAME}.'
"""


"""
def handle_upload(filename, filecontents):
    # Handle the file upload
    pass

# Define the callback to handle the submit button click


@app.callback(Output('output', 'children'),
              Input('submit', 'n_clicks'),
              State('input', 'value'))
def handle_submit(n_clicks, input_value):
    # Handle the form submission
    pass
"""
# SHIT CODE BUT ALSO HAS MANTINE PROVIDER FOR FUTURE

# app.layout = dmc.MantineProvider(
#     theme={"colorScheme": "dark"},
#     # inherit=True,
#     # withGlobalStyles=True,
#     # withNormalizeCSS=True,
#     children=[
#         dmc.TextInput(id='input', label='Enter a SMILES string',
#                          placeholder='e.g. C1=CC=CC=C1C=O'),
#         dmc.MantineFileInput(id='upload', label='Upload your files', accept=[
#                              '.bam', '.sam', '.vcf'], multiple=True),
#         dmc.MantineButton(children='Submit', id='submit'),
#         # Add your output components here

#     ]
# )
# , html.Div([  # id='output')
#     dcc.Input(id='input', type='text', placeholder='Enter a SMILES string'),
#     dcc.Upload(id='upload', children=html.Div(
#         ['Drag and Drop or ', html.A('Select Files')]), multiple=True),
# ])
# app.layout = html.Div([
#     # Add your input components here
# dcc.Input(id='input', type='text', placeholder='Enter a SMILES string'),
# dcc.Upload(id='upload', children=html.Div(
#     ['Drag and Drop or ', html.A('Select Files')]), multiple=True),
#     html.Button('Submit', id='submit'),
#     # Add your output components here
#     html.Div(id='output')
# ])

# the following for dev/debugging in command line:
# export FLASK_APP=app
# flask run
